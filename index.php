<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title> Inventory Management </title>
</head>
<body>

	<?php

    ob_start();
    session_start();
    include("connect.php");

    $sql = "SELECT * FROM product";
    $query = mysqli_query($con, $sql);

    ?>

    <ul class="nav justify-content-end" id="a">
    	<li class="nav-item">
    		<a class="nav-link" href="index.php">Product</a>
    	</li>

      	<li class="nav-item">
        	<a class="nav-link active" href="category.php">Categories</a>
        </li>
  	</ul>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="mt-5">Product</h1>
          <a href="add-product.php" class="btn btn-secondary btn-sm">Add Product</a>

          <table  id="product" class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Category</th>
              <th>Name</th>
              <th>Short Description</th>
              <th>Long Description</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php

           	   while($row = mysqli_fetch_array($query))
              {
                echo '<tr>';
                echo '<td>' . $row['id']. '</td>';
                echo '<td>' . $row['cat_id']. '</td>';
                echo '<td>' . $row['name']. '</td>';
                echo '<td>' . $row['short_desc']. '</td>';
                echo '<td>' . $row['long_desc']. '</td>';

                echo '<td>' . '<a class="btn btn-danger" href="delete-product.php? del=<?'. $row['id'] . '" role="button">Delete</a>' . '</td>';
                echo '</tr>';
              }
          	
          	
          	mysqli_close($con)

            ?>
             
          </tbody>
        </table>
        <?php
        	if(($row = mysqli_fetch_array($query)) < 1)
            { 
             
            }
            else
          	{
          		echo '<div class="jumbotron jumbotron-fluid">
  						<h1 class="display-4" style="text-align: center">There are no product at this time.</h1>		
					  </div>';
             
          	}
        ?>
        </div>
      </div>
    </div>
</body>
</html>