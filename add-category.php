<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title> Inventory Management </title>
</head>
<body>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="mt-5">Add Category</h3>

          <div class="col-lg-8">
          
            <form method="post" value="">
              <div class="form-group">
                <label for="name">Name</label>
                <input name="name" type="text" class="form-control" placeholder="Enter Category Name" required>
              </div>

              <div class="form-group">
                <label for="description">Description</label>
                <input name="description" type="text"  class="form-control"  placeholder="Enter Category Description" required>
              </div>

              <button type="submit" name="add-category" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <?php

    ob_start();
    session_start();
    include("connect.php");

    if (isset($_POST['add-category']))
    {
      $name = $_POST['name'];
      $description= $_POST['description'];
      
      $sql = "INSERT INTO product_category (name, description) VALUES ('$name', '$description')"; 
      mysqli_query($con, $sql);
      mysqli_close($con);
      echo '<div class="alert alert-info alert-dismissible fade show" role="alert"> ';
      echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"> ';
      echo '<span aria-hidden="true">&times;</span> ';
      echo '</button>';
      echo '<strong>Successfully added new category!</strong>';
      echo '</div>';

    }

    ?>

</body>
</html>