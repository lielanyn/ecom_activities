<!DOCTYPE html>
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

		<title> Inventory Management </title>

</head>
<body>

	<ul class="nav justify-content-end" id="a">
  		<li class="nav-item">
    		<a class="nav-link active" href="index.php">Product</a>
  		</li>
  		<li class="nav-item">
    		<a class="nav-link" href="category.php">Categories</a>
  		</li>
	</ul> 

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="mt-5">Add Product</h1>
          <a href="product.php" class="btn btn-secondary btn-sm">Back to Product List</a><hr>

          <div class="col-lg-8">

            <!-- SHOW AN ALERT MESSAGE IF A USER SUCCESSFULLY ADDED A PRODUCT -->
            <?php if (!empty($success_message)): ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?= $success_message ?>
              </div>
            <?php endif; ?>

            <!-- SHOW AN ALERT MESSAGE IF A USER FAILED TO ADD A PRODUCT -->
            <?php if (!empty($error_message)): ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <?= $error_message ?>
              </div>
            <?php endif; ?>

            <form enctype="multipart/form-data" method="post" value="">
             
              <div class="form-group">
                <label for="name">Name</label>
                <input name="name" type="text" class="form-control" placeholder="Enter Product Name" required>
              </div>

              <div class="form-group">
                <label for="category">Category</label>
                
                	<?php
              			ob_start();
						        session_start();
						        include("connect.php");

						        $query = "SELECT * FROM product_category";
						        $result = mysqli_query($con, $query);
						        $opt = "";

						        while($row = mysqli_fetch_array($result))
						        {
							         $opt = $opt."<option>$row[1]</option>";
						        }             		
             		?>
             	<select name="category_id" class="form-control">
             		<?php echo $opt; ?>
                </select>
              </div>

              <div class="form-group">
                <label for="short_desc">Short Description</label>
                <input name="short_desc" type="text" class="form-control"  placeholder="Short description for the product" required>
              </div>

              <div class="form-group">
                <label for="long_desc">Long Description</label>
                <input name="long_desc" type="text" class="form-control"  placeholder="Long description for the product" required>
              </div>

              <button type="submit" name="add-product" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <?php

    if (isset($_POST['add-product']))
	  {
		  $name = $_POST['name'];
      $category_id= $_POST['category_id'];
  		$short_desc= $_POST['short_desc'];
      $long_desc= $_POST['long_desc'];

			$sql = "INSERT INTO product (cat_id, name, short_desc, long_desc) VALUES ('$category_id', '$name', '$short_desc', '$long_desc')"; 
			mysqli_query($con, $sql);
			mysqli_close($con);
			echo '<div class="alert alert-info alert-dismissible fade show" role="alert"> ';
  		echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"> ';
    	echo '<span aria-hidden="true">&times;</span> ';
  		echo '</button>';
  		echo '<strong>Successfully added new product!</strong>';
			echo '</div>';
	}

    ?>

</body>
</html>